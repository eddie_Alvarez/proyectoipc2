﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Administrative_Management.DB
{
    public class Connection_DB
    {

        //Conexion a la base de datos
        private static SqlConnection connection = new SqlConnection("Data Source=EDDIE; Initial Catalog=DB_Administrative_Management; Integrated Security=True");

        public static SqlConnection getConnection()
        {

            return connection;

        }

        public SqlCommand AddCommand(String instruccions)
        {

            SqlCommand command = new SqlCommand(instruccions, connection);
            return command;

        }

    }
}