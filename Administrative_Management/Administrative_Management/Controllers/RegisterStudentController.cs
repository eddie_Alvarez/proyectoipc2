﻿using Administrative_Management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Administrative_Management.Controllers
{
    public class RegisterStudentController : Controller
    {
        // GET: RegisterStudent
        public ActionResult RegisterStudentView()
        {
            User user = new User();
            return View();
        }

        public ActionResult AddUser() {

            if (Request.Form["btnCancel"] != null) {
                return RedirectToAction("../Login/LoginView");
            }
            User user = new User();
            user.license = Convert.ToInt32(Request.Form["txtLicense"].ToString());
            user.name = Request.Form["txtName"].ToString();
            user.lastName = Request.Form["txtlastName"].ToString();
            user.birthdate = Request.Form["txtBirthdate"].ToString();
            user.email = Request.Form["txtEmail"].ToString();
            user.phone = Convert.ToInt32(Request.Form["txtPhone"].ToString());
            user.username = Request.Form["txtUsername"].ToString();
            user.passsword = Request.Form["txtPassword"].ToString();
            user.keyword = Request.Form["txtKeyword"].ToString();
            user.role = 3;
            if (user.passsword.Equals(Request.Form["txtPasswordConfirm"].ToString()))
            {
                if (user.InsertUser())
                {
                    TempData["Message"] = null;
                    return RedirectToAction("../Login/LoginView");
                }
                else
                {
                    TempData["Message"] = "Error al registrarse";
                    return View("RegisterStudentView");
                }
            }
            else {
                TempData["Message"] = "Las contraseñas no coinciden";
                return View("RegisterStudentView", user);
            }
        }
    }
}