﻿using Administrative_Management.Models;
using MessagingToolkit.QRCode.Codec;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Administrative_Management.Controllers
{
    public class PanelOperatorController : Controller
    {
        // GET: PanelOperator
        public ActionResult PanelOperatorView()
        {
            return View();
        }

        public ActionResult AdminSalonView()
        {
            Salon salonPivot = new Salon();
            List<Salon> salonList = salonPivot.ListSalon();
            ViewBag.list = salonList;
            return View();
        }

        public ActionResult CreateSalonView()
        {
            return View();
        }

        public ActionResult AddSalon()
        {
            if (Request.Form["btnCancel"] != null)
            {
                return RedirectToAction("AdminSalonView");
            }
            Salon salon = new Salon();
            salon.number = Request.Form["txtNumber"].ToString();
            salon.location = Request.Form["txtLocation"].ToString();
            salon.status = 1;
            if (salon.InsertSalon())
            {
                ViewBag.error = null;
                return RedirectToAction("AdminSalonView");
            }
            else
            {
                ViewBag.error = "Error al crear salon";
                return View("CreateSalonView");
            }
        }

        public ActionResult SelectSalon(int id)
        {
            Salon salon = new Salon();
            salon = salon.SelectSalon(id);
            return View("EditSalonView", salon);
        }

        public ActionResult EditSalon() {
            if (Request.Form["btnCancel"] != null)
            {
                return RedirectToAction("AdminSalonView");
            }
            Salon salon = new Salon();
            salon.id = Convert.ToInt32(Request.Form["txtId"]);
            salon.number = Request.Form["txtNumber"].ToString();
            salon.location = Request.Form["txtLocation"].ToString();
            salon.status = Convert.ToInt32(Request.Form["cmbStatus"]);
            if (salon.EditSalon())
            {
                ViewBag.error = null;
                return RedirectToAction("AdminSalonView");
            }
            else
            {
                ViewBag.error = "Error al editar el usuario";
                return View("EditSalonView", salon);
            }
        }

        public ActionResult DeleteSalon(int id) {
            Salon salon = new Salon();
            if (salon.DeleteSalon(id))
            {
                ViewBag.error = null;
                return RedirectToAction("AdminSalonView");
            }
            else {
                ViewBag.error = "Error al eliminar el salon";
                return RedirectToAction("AdminSalonView");
            }
        }

        public ActionResult AdminReservationView() {
            Reservation reservationPivot = new Reservation();
            List<Reservation> reservationList = reservationPivot.ListReservation();
            ViewBag.list = reservationList;
            return View();
        }

        public ActionResult CreateReservationView() {
            User user = new User();
            Salon salon = new Salon();
            List<User> userList = new List<User>();
            List<Salon> salonList = new List<Salon>();
            userList = user.SearchReservation();
            salonList = salon.SearchReservation();
            ViewBag.userList = userList;
            ViewBag.salonList = salonList;
            return View();
        }

        public ActionResult AddReservation() {
            Reservation reservation = new Reservation();
            Salon salon = new Salon();
            reservation.hour = Request.Form["txtHour"].ToString();
            reservation.date = Request.Form["txtDate"].ToString();
            reservation.status = 1;
            reservation.activity = Request.Form["txtActivity"].ToString();
            reservation.Operator = Convert.ToInt32(Session["user"]);
            reservation.instructor = Convert.ToInt32(Request.Form["cmbInstructor"]);
            reservation.salon = Convert.ToInt32(Request.Form["cmbSalon"]);
            if (reservation.InsertReservation())
            {
                if (salon.ChangeStatusReservation(reservation.salon))
                {
                    ViewBag.error = null;
                    return RedirectToAction("AdminReservationView");
                }
                else {
                    ViewBag.error = "Error al crear reservacion";
                    return RedirectToAction("CreateReservationView");
                }
            }
            else {
                ViewBag.error = "Error al crear reservacion";
                return RedirectToAction("CreateReservationView");
            }
        }

        public ActionResult SelectReservation(int id)
        {
            Reservation reservation = new Reservation();
            reservation = reservation.SelectReservation(id);
            User user = new User();
            Salon salon = new Salon();
            List<User> userList = new List<User>();
            List<Salon> salonList = new List<Salon>();
            userList = user.SearchReservation();
            salonList = salon.SearchReservation();
            ViewBag.userList = userList;
            ViewBag.salonList = salonList;
            return View("EditReservationView", reservation);
        }

        public ActionResult EditReservation()
        {
            if (Request.Form["btnCancel"] != null)
            {
                return RedirectToAction("AdminReservationView");
            }
            Reservation reservation = new Reservation();
            reservation.id = Convert.ToInt32(Request.Form["txtId"]);
            reservation.hour = Request.Form["txtHour"].ToString();
            reservation.date = Request.Form["txtDate"].ToString();
            reservation.letter = Request.Form["fileUpload"].ToString();
            if (reservation.letter != "" && reservation.letter != null)
            {
                reservation.status = 0;
                QRCodeEncoder encoder = new QRCodeEncoder();
                Bitmap img = encoder.Encode("Endode Text");
                Image qr = img;

                using (MemoryStream ms = new MemoryStream()) {
                    qr.Save(ms, ImageFormat.Jpeg);
                    byte[] imageBytes = ms.ToArray();
                    String src = "data:image/gif;base64," + Convert.ToBase64String(imageBytes);
                    reservation.codeQR = src;
                }
                    
            }
            else
            {
                reservation.status = Convert.ToInt32(Request.Form["cmbStatus"]);
            }
            reservation.activity = Request.Form["txtActivity"].ToString();
            reservation.instructor = Convert.ToInt32(Request.Form["cmbInstructor"]);
            reservation.salon = Convert.ToInt32(Request.Form["cmbSalon"]);
            if (reservation.EditReservation())
            {
                ViewBag.error = null;
                return RedirectToAction("AdminReservationView");
            }
            else
            {
                ViewBag.error = "Error al editar la reservacion";
                return View("EditReservationView", reservation);
            }
        }

        public ActionResult DeleteReservation(int id)
        {
            Reservation reservation = new Reservation();
            if (reservation.DeleteReservation(id))
            {
                ViewBag.error = null;
                return RedirectToAction("AdminReservationView");
            }
            else
            {
                ViewBag.error = "Error al eliminar la reservacion";
                return RedirectToAction("AdminReservationView");
            }
        }

        public ActionResult Logout() {
            Session["user"] = null;
            Session["username"] = null;
            return RedirectToAction("../Login/LoginView");
        }

        public ActionResult CreateIncidentView() {
            Reservation reservation = new Reservation();
            TypeIncident type = new TypeIncident();
            ViewBag.reservationList = reservation.ToListSalon();
            ViewBag.typeList = type.ToList();
            return View();
        }

        public ActionResult AdminIncidentView()
        {
            Incident incident = new Incident();
            ViewBag.list = incident.ListIncident();
            return View();
        }

        public ActionResult AddIncident()
        {
            if (Request.Form["btnCancel"] != null)
            {
                return RedirectToAction("AdminIncidentView");
            }
            Reservation reservation = new Reservation();
            Incident incident = new Incident();
            incident.description = Request.Form["txtDescription"].ToString();
            incident.status = 1;
            incident.type = Convert.ToInt32(Request.Form["cmbType"]);
            reservation.id = Convert.ToInt32(Request.Form["cmbSalon"]);
            int id = incident.InsertIncident();
            if (id != 0)
            {
                if (reservation.EditReservationIncident(id))
                {
                    ViewBag.error = null;
                    return RedirectToAction("AdminIncidentView");
                }
                else {
                    ViewBag.error = "Error al reportar incidente";
                    return RedirectToAction("CreateIncidentView");
                }
            }
            else
            {
                ViewBag.error = "Error al reportar incidente";
                return RedirectToAction("CreateIncidentView");
            }
        }

        public ActionResult AdminInputView() {
            Input input = new Input();
            ViewBag.list = input.ListInput();
            return View();
        }

        public ActionResult CreateInputView() {
            return View();
        }

        public ActionResult AddInput() {
            if (Request.Form["btnCancel"] != null)
            {
                return RedirectToAction("AdminInputView");
            }
            Input input = new Input();
            input.name = Request.Form["txtName"].ToString();
            input.brand = Request.Form["txtBrand"].ToString();
            input.model = Request.Form["txtModel"].ToString();
            input.status = 1;
            input.type = Convert.ToInt32(Request.Form["cmbType"]);
            if (input.InsertInput())
            {
                ViewBag.error = null;
                return RedirectToAction("AdminInputView");
            }
            else
            {
                ViewBag.error = "Error al crear un insumo";
                return RedirectToAction("CreateInputView");
            }
        }

        public ActionResult SelectInput(int id) {
            Input input = new Input();
            input = input.SelectInput(id);
            return View("EditInputView", input);
        }

        public ActionResult EditInput()
        {
            if (Request.Form["btnCancel"] != null)
            {
                return RedirectToAction("AdminInputView");
            }
            Input input  = new Input();
            input.id = Convert.ToInt32(Request.Form["txtId"]);
            input.name = Request.Form["txtName"].ToString();
            input.brand = Request.Form["txtBrand"].ToString();
            input.model = Request.Form["txtModel"].ToString();
            input.status = Convert.ToInt32(Request.Form["cmbStatus"]);
            input.type = Convert.ToInt32(Request.Form["cmbType"]);
            if (input.EditInput())
            {
                ViewBag.error = null;
                return RedirectToAction("AdminInputView");
            }
            else
            {
                ViewBag.error = "Error al editar el Insumo";
                return View("EditInputView", input);
            }
        }

        public ActionResult DeleteInput(int id)
        {
            Input input = new Input();
            if (input.DeleteInput(id))
            {
                ViewBag.error = null;
                return RedirectToAction("AdminInputView");
            }
            else
            {
                ViewBag.error = "Error al eliminar el Insumo";
                return RedirectToAction("AdminInputView");
            }
        }

        public ActionResult LendInputView() {
            Input input = new Input();
            ViewBag.list = input.ListLendInput();
            return View();
        }

        public ActionResult CreateLendView(int id) {
            Input input = new Input();
            User user = new User();
            ViewBag.idInput = id;
            ViewBag.Listuser = user.ListUserForLend();
            ViewBag.name = input.SelectNameInput(id);
            return View();
        }

        public ActionResult AddLend() {
            if (Request.Form["btnCancel"] != null)
            {
                return RedirectToAction("LendInputView");
            }
            Input input = new Input();
            LendInput lend = new LendInput();
            lend.status = 1;
            lend.usuario = Convert.ToInt32(Request.Form["cmbUser"]);
            lend.input = Convert.ToInt32(Request.Form["txtidInput"]); 
            if (lend.InsertLendInput())
            {
                if (input.ChangeStatusLend(lend.input))
                {
                    ViewBag.error = null;
                    return RedirectToAction("LendInputView");
                }
                else {
                    ViewBag.error = "Error al cambiar de estado el insumo";
                    return RedirectToAction("LendInputView");
                }
            }
            else
            {
                ViewBag.error = "Error al crear un insumo";
                return RedirectToAction("CreateLendView", lend.input);
            }
        }

        public ActionResult ReturnInput(int id) {
            Input input = new Input();
            LendInput lend = new LendInput();
            input.ChangeStatusReturn(id);
            lend.ChangeStatusReturn(id);
            return RedirectToAction("LendInputView");
        }

        public ActionResult ReportInput(int id)
        {
            Input input = new Input();
            TypeIncident type = new TypeIncident();
            ViewBag.typeList = type.ToList();
            ViewBag.id = id;
            ViewBag.name = input.SelectNameInput(id);
            return View("CreateIncidentLendView");
        }

        public ActionResult AddIncidentLend()
        {
            Input input = new Input();
            LendInput lend = new LendInput();
            input.id = Convert.ToInt32(Request.Form["txtIdinput"]);
            Incident incident = new Incident();
            incident.status = 1;
            incident.description = Request.Form["txtDescription"];
            incident.type = Convert.ToInt32(Request.Form["cmbType"]);
            int id = incident.InsertIncident();
            if (id != 0)
            {
                if (input.EditInputIncident(id))
                {
                    if (input.ChangeStatusIncident())
                    {
                        if (lend.ChangeStatusIncident(input.id))
                        {
                            ViewBag.error = null;
                            return RedirectToAction("LendInputView");
                        }
                        else
                        {
                            ViewBag.error = "Error al cambiar de estado el insumo";
                            return RedirectToAction("LendInputView");
                        }
                    }
                    else
                    {
                        ViewBag.error = "Error al cambiar de estado el insumo";
                        return RedirectToAction("LendInputView");
                    }
                }
                else
                {
                    ViewBag.error = "Error al reportar incidente";
                    return RedirectToAction("CreateIncidentLendView");
                }
            }
            else
            {
                ViewBag.error = "Error al reportar incidente";
                return RedirectToAction("CreateIncidentLendView");
            }
        }

    }
}