﻿using Administrative_Management.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Administrative_Management.Controllers
{
    public class PanelInstructorController : Controller
    {
        // GET: PanelInstructor
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PanelInstructorView() {
            return View();
        }

        public ActionResult Logout()
        {
            Session["user"] = null;
            Session["username"] = null;
            return RedirectToAction("../Login/LoginView");
        }

        public ActionResult ViewReservation() {
            Reservation reservation = new Reservation();
            ViewBag.ListReservationCompleted = reservation.ListReservationCompleted(Convert.ToInt32(Session["user"]));
            return View();
        }

        public ActionResult DowloadQRView(int id) {
            Reservation reservation = new Reservation();
            String qr = reservation.getQR(id);
            ViewBag.src = qr;
            ViewBag.width = 200;
            ViewBag.height = 200;
            return View();
        }

        public ActionResult UploadPresentationView(int id)
        {
            ViewBag.id = id;
            return View();
        }

        public ActionResult addPresentation()
        {
            Reservation reservation = new Reservation();
            reservation.id = Convert.ToInt32(Request.Form["txtIdReservation"]);
            HttpPostedFileBase postedFile = Request.Files["uploadFile"];
            if (postedFile != null && postedFile.ContentLength > 0)
            {
                reservation.presentation = Server.MapPath("~/Uploads/" + Path.GetFileName(postedFile.FileName));
                postedFile.SaveAs(reservation.presentation);
            }
            if (reservation.EditReservationUploadPresentation())
            {
                ViewBag.error = null;
                return RedirectToAction("ViewReservation");
            }
            else
            {
                ViewBag.error = "Error al subir presentacion";
                return RedirectToAction("UploadPresentationView");
            }
        }

        public ActionResult UploadQuestionnaireView(int id)
        {
            ViewBag.id = id;
            return View();
        }

        public ActionResult addQuestionnaire()
        {
            Reservation reservation = new Reservation();
            reservation.id = Convert.ToInt32(Request.Form["txtidReservation"]);
            reservation.questionnaire = Request.Form["txtQuestionnaire"].ToString();
            if (reservation.EditReservationQuestionnaire())
            {
                ViewBag.error = null;
                return RedirectToAction("ViewReservation");
            }
            else
            {
                ViewBag.error = "Error al subir presentacion";
                return RedirectToAction("UploadQuestionnaireView");
            }
        }

        public ActionResult DownLoadQr()
        {
            WebClient client = new WebClient();
            String name = Request.Form["imgQr"].ToString();
            Stream stream = client.OpenRead(Request.Form["imgQr"]);
            Bitmap bitmap = new Bitmap(stream);
            stream.Flush();
            stream.Close();
            client.Dispose();
            if (bitmap != null)
            {
                bitmap.Save("C:\\" + "qr.jpg");
            }
            return RedirectToAction("ViewReservation");
        }

    }
}