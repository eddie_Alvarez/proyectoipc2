﻿using Administrative_Management.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Administrative_Management.Controllers
{
    public class PanelAdminController : Controller
    {
        // GET: PanelAdmin
        public ActionResult PanelAdminView()
        {
            return View();
        }

        public ActionResult AdminUsersView()
        {
            User userPivot = new User();
            List<User> userList = userPivot.ListUser();
            ViewBag.list = userList;
            return View();
        }

        public ActionResult CreateUserView()
        {
            return View();
        }

        public ActionResult Adduser()
        {
            if (Request.Form["btnCancel"] != null)
            {
                return RedirectToAction("AdminUsersView");
            }
            User user = new User();
            user.license = Convert.ToInt32(Request.Form["txtLicense"].ToString());
            user.name = Request.Form["txtName"].ToString();
            user.lastName = Request.Form["txtlastName"].ToString();
            user.birthdate = Request.Form["txtBirthdate"].ToString();
            user.email = Request.Form["txtEmail"].ToString();
            user.phone = Convert.ToInt32(Request.Form["txtPhone"].ToString());
            user.username = Request.Form["txtUsername"].ToString();
            user.passsword = Request.Form["txtPassword"].ToString();
            user.keyword = Request.Form["txtKeyword"].ToString();
            user.role = Convert.ToInt32(Request.Form["cmbRole"]);
            if (user.passsword.Equals(Request.Form["txtPasswordConfirm"].ToString()))
            {
                if (user.InsertUser())
                {
                    ViewBag.error = null;
                    return RedirectToAction("AdminUsersView");
                }
                else
                {
                    ViewBag.error = "Error al crear el usuario";
                    return View("CreateUserView");
                }
            }
            else
            {
                ViewBag.error = "Las contraseñas no coinciden";
                return View("CreateUserView", user);
            }
        }
        public ActionResult SelectUser(int id)
        {
            User user = new User();
            user = user.SelectUser(id);
            return View("EditUserView", user);
        }

        public ActionResult EditUser()
        {
            if (Request.Form["btnCancel"] != null)
            {
                return RedirectToAction("AdminUsersView");
            }
            User user = new User();
            user.license = Convert.ToInt32(Request.Form["txtLicense"].ToString());
            user.name = Request.Form["txtName"].ToString();
            user.lastName = Request.Form["txtlastName"].ToString();
            user.birthdate = Request.Form["txtBirthdate"].ToString();
            user.email = Request.Form["txtEmail"].ToString();
            user.phone = Convert.ToInt32(Request.Form["txtPhone"].ToString());
            user.username = Request.Form["txtUsername"].ToString();
            if (user.EditUser())
            {
                ViewBag.error = null;
                return RedirectToAction("AdminUsersView");
            }
            else
            {
                ViewBag.error = "Error al editar el usuario";
                return View("EditUserView", user);
            }
        }
        public ActionResult DeleteUser(int id) {
            User user = new User();
            if (user.DeleteUser(id))
            {
                ViewBag.error = null;
                return RedirectToAction("AdminUsersView");
            }
            else {
                ViewBag.error = "Error al eliminar el usuario";
                return RedirectToAction("AdminUsersView");
            }
        }

        public ActionResult Logout()
        {
            Session["user"] = null;
            Session["username"] = null;
            return RedirectToAction("../Login/LoginView");
        }

        public ActionResult Upload(HttpPostedFile File) {
            return View();
        }

        public ActionResult ChargeView()
        {
            return View();
        }

        public ActionResult UploadFile()
        {
            String fileName = "C:\\Users\\Eddie\\Documents";
            if (Request.Form["uploadFile"].ToString() != "") {
                //String fileName = Request.Form["uploadFile"].ToString();
            }
            return View();
        }
    }
}