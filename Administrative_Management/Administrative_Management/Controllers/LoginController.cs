﻿using Administrative_Management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Administrative_Management.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult LoginView()
        {
            return View();
        }

        public ActionResult LoginUser() {
            User user = new User();
            user.username = Request.Form["txtUser"].ToString();
            String password = Request.Form["txtPassword"].ToString();
            user.passsword = user.LoginUser();
            if (user.passsword != null)
            {
                if (password.Equals(user.passsword))
                {
                    Session["user"] = user.license;
                    Session["username"] = user.username;
                    switch (user.role) {
                        case 1:
                            return RedirectToAction("../PanelAdmin/PanelAdminView");
                        case 2:
                            return RedirectToAction("../PanelOperator/PanelOperatorView");
                        case 3:
                            return RedirectToAction("../PanelStudent/PanelStudentView");
                        case 4:
                            return RedirectToAction("../PanelInstructor/PanelInstructorView");
                    }
                }
                else {
                    ViewBag.error = "Credenciales incorrectos";
                    return RedirectToAction("LoginView");
                }
            }
             ViewBag.error = "Credenciales incorrectos";
             return View("LoginView");
        }

        public ActionResult RecoverPasswordView() {
            return View();
        }

        public ActionResult RecoverPassword() {
            if (Request.Form["btnCancel"] != null)
            {
                return RedirectToAction("LoginView");
            }
            User user = new User();
            user.license = Convert.ToInt32(Request.Form["txtLicense"]);
            user.keyword = Request.Form["txtKeyword"].ToString();
            User userPivot = user.RecoverPassword();
            if (user.keyword.Equals(userPivot.keyword))
            {
                ViewBag.error = null;
                MailAddress fromAddress = new MailAddress("eddirazaugusto@gmail.com", "Eddie Alvarez");
                MailAddress toAddress = new MailAddress(userPivot.email, userPivot.name);
                String fromPassword = "18Emirates";
                String subject = "Recuperacion de contraseña";
                String body = "Su contraseña es " + userPivot.passsword;
                SmtpClient smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (MailMessage message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                }) {
                    smtp.Send(message);
                }
                    return View("LoginView");
            }
            else {
                ViewBag.error = "Carnet o palabra clave invalida";
                return View("RecoverPasswordView");
            }
        }

        public ActionResult ChangePassword() {
            if (Request.Form["btnCancel"] != null)
            {
                return RedirectToAction("LoginView");
            }
            User user = new User();
            user.license = Convert.ToInt32(Request.Form["txtLicense"]);
            user.passsword = Request.Form["txtPassword"].ToString();
            if (user.passsword.Equals(Request.Form["txtConfirmPassword"].ToString()))
            {
                if (user.ChangePassword())
                {
                    ViewBag.error = null;
                    return View("LoginView");
                }
                else {
                    ViewBag.error = "Error al cambiar la contraseña";
                    return View("ChangePasswordView", user);
                }
            }
            else {
                ViewBag.error = "Las contraseñas no coinciden";
                return View("ChangePasswordView", user);
            }
        }
    }
}