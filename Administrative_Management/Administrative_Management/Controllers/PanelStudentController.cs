﻿using Administrative_Management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Administrative_Management.Controllers
{
    public class PanelStudentController : Controller
    {
        // GET: PanelStudent
        public ActionResult PanelStudentView()
        {
            return View();
        }

        public ActionResult Logout()
        {
            Session["user"] = null;
            Session["username"] = null;
            return RedirectToAction("../Login/LoginView");
        }

        public ActionResult ActivitiesView()
        {
            Reservation reservation = new Reservation();
            ViewBag.ListReservationCompleted = reservation.ActivitiesList();
            return View();
        }

        public ActionResult Enroll(int id)
        {
            Reservation reservation = new Reservation();
            reservation.id = id;
            if (reservation.InsertEnroll(Convert.ToInt32(Session["user"])))
            {
                ViewBag.error = null;
                return RedirectToAction("ActivitiesView");
            }
            else
            {
                ViewBag.error = "Error al eliminar el salon";
                return RedirectToAction("ActivitiesView");
            }
        }
    }
}