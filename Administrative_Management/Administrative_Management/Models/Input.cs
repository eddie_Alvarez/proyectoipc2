﻿using Administrative_Management.DB;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Administrative_Management.Models
{
    public class Input
    {

        public int id { get; set; }
        public String name { get; set; }
        public String brand { get; set; }
        public String model { get; set; }
        public int status { get; set; }
        public int type { get; set; }
        public String nameTyoe { get; set; }


        public List<Input> ListInput()
        {

            SqlConnection connection = Connection_DB.getConnection();
            List<Input> inputList = new List<Input>();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Input";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                while (data.Read()) {
                    Input input = new Input();
                    input.id = Convert.ToInt32(data["id"]);
                    input.name = data["name"].ToString();
                    input.brand = data["brand"].ToString();
                    input.model = data["model"].ToString();
                    input.status = Convert.ToInt32(data["status"]);
                    input.type = Convert.ToInt32(data["type"]);
                    inputList.Add(input);
                }
                connection.Close();
                return inputList;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public Boolean InsertInput() {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "INSERT INTO Input(name, brand, model, status, type)"
                                + "VALUES (@1,@2,@3, @4, @5)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", name );
                command.Parameters.AddWithValue("@2", brand);
                command.Parameters.AddWithValue("@3", model);
                command.Parameters.AddWithValue("@4", status);
                command.Parameters.AddWithValue("@5", type);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Input SelectInput(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Input WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                SqlDataReader data = command.ExecuteReader();
                Input input = new Input();
                while (data.Read())
                {
                    input.id = Convert.ToInt32(data["id"]);
                    input.name = data["name"].ToString();
                    input.brand = data["brand"].ToString();
                    input.model = data["model"].ToString();
                    input.status = Convert.ToInt32(data["status"]);
                    input.type = Convert.ToInt32(data["type"]);
                }
                connection.Close();
                return input;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean EditInput()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE Input SET name = @1, brand = @2, model = @3, status = @4, type = @5 WHERE id = @6";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", name);
                command.Parameters.AddWithValue("@2", brand);
                command.Parameters.AddWithValue("@3", model);
                command.Parameters.AddWithValue("@4", status);
                command.Parameters.AddWithValue("@5", type);
                command.Parameters.AddWithValue("@6", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Boolean DeleteInput(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "DELETE FROM Input WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<Input> ListLendInput()
        {

            SqlConnection connection = Connection_DB.getConnection();
            List<Input> inputList = new List<Input>();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Input WHERE status = 1 OR status = 0";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                while (data.Read())
                {
                    Input input = new Input();
                    input.id = Convert.ToInt32(data["id"]);
                    input.name = data["name"].ToString();
                    input.brand = data["brand"].ToString();
                    input.model = data["model"].ToString();
                    input.status = Convert.ToInt32(data["status"]);
                    input.type = Convert.ToInt32(data["type"]);
                    inputList.Add(input);
                }
                connection.Close();
                return inputList;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public String SelectNameInput(int id) {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT name FROM Input WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                String name = command.ExecuteScalar().ToString();
                connection.Close();
                return name;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean ChangeStatusLend(int id) {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE Input SET status = 0 WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Boolean ChangeStatusReturn(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE Input SET status = 1 WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Boolean EditInputIncident(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE LendInput SET incident = @1, status = 2 WHERE id = @2";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                command.Parameters.AddWithValue("@2", this.id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Boolean ChangeStatusIncident()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE Input SET status = 2 WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}