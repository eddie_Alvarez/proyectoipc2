﻿using Administrative_Management.DB;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Administrative_Management.Models
{
    public class Incident
    {
        public int id { get; set; }
        public String description { get; set; }
        public int status { get; set; }
        public int type { get; set; }

        public int InsertIncident()
        {

            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "INSERT INTO Incident(description, status, type)"
                                + "VALUES (@1,@2,@3)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", description);
                command.Parameters.AddWithValue("@2", status);
                command.Parameters.AddWithValue("@3", type);
                command.ExecuteNonQuery();
                String sql2 = "SELECT @@IDENTITY";
                SqlCommand command2 = new SqlCommand(sql2, connection);
                int id = Convert.ToInt32(command2.ExecuteScalar());
                connection.Close();
                return id;
            }
            catch (Exception e)
            {
                return 0;
            }

        }

        public List<Incident> ListIncident()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Incident";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                List<Incident> incidentList = new List<Incident>();
                while (data.Read())
                {
                    Incident incident = new Incident();
                    incident.id = Convert.ToInt32(data["id"]);
                    incident.description = data["description"].ToString();
                    incident.status = Convert.ToInt32(data["status"]);
                    incident.type = Convert.ToInt32(data["type"]);
                    incidentList.Add(incident);
                }
                connection.Close();
                return incidentList;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}