﻿using Administrative_Management.DB;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Administrative_Management.Models
{
    public class Reservation
    {
        public int id { get; set; }
        public String hour { get; set; }
        public String date { get; set; }
        public String codeQR { get; set; }
        public int status { get; set; }
        public String letter { get; set; }
        public String activity { get; set; }
        public String questionnaire { get; set; }
        public String presentation { get; set; }
        public int Operator { get; set; }
        public int instructor { get; set; }
        public int salon { get; set; }
        public int incident { get; set; }

        public Boolean InsertReservation() {
            SqlConnection connection = Connection_DB.getConnection();
            try {
                connection.Open();
                String sql = "INSERT INTO Reservation(hora, fecha, status, activity, operator, instructor, salon)"
                                + "VALUES (@1,@2,@3,@4,@5,@6,@7)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", hour);
                command.Parameters.AddWithValue("@2", date);
                command.Parameters.AddWithValue("@3", status);
                command.Parameters.AddWithValue("@4", activity);
                command.Parameters.AddWithValue("@5", Operator);
                command.Parameters.AddWithValue("@6", instructor);
                command.Parameters.AddWithValue("@7", salon);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e) {
                return false;
            }
        }

        public List<Reservation> ListReservation()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Reservation";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                List<Reservation> reservationList = new List<Reservation>();
                while (data.Read())
                {
                    Reservation reservation = new Reservation();
                    reservation.id = Convert.ToInt32(data["id"]);
                    reservation.hour = data["hora"].ToString();
                    reservation.date = data["fecha"].ToString();
                    reservation.status = Convert.ToInt32(data["status"]);
                    reservation.activity = data["activity"].ToString();
                    reservation.Operator = Convert.ToInt32(data["operator"]);
                    reservation.instructor = Convert.ToInt32(data["instructor"]);
                    reservation.salon = Convert.ToInt32(data["salon"]);
                    reservationList.Add(reservation);
                }
                connection.Close();
                return reservationList;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Reservation SelectReservation(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Reservation WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                SqlDataReader data = command.ExecuteReader();
                Reservation reservation = new Reservation();
                while (data.Read())
                {
                    reservation.id = Convert.ToInt32(data["id"]);
                    reservation.hour = data["hora"].ToString();
                    reservation.date = data["fecha"].ToString();
                    reservation.status = Convert.ToInt32(data["status"]);
                    reservation.activity = data["activity"].ToString();
                    reservation.Operator = Convert.ToInt32(data["operator"]);
                    reservation.instructor = Convert.ToInt32(data["instructor"]);
                    reservation.salon = Convert.ToInt32(data["salon"]);
                }
                connection.Close();
                return reservation;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean EditReservation()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE Reservation SET hora = @1, fecha = @2, status = @3, activity = @4, instructor = @5, salon = @6, letter = @7, qr = @8 WHERE id = @9";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", hour);
                command.Parameters.AddWithValue("@2", date);
                command.Parameters.AddWithValue("@3", status);
                command.Parameters.AddWithValue("@4", activity);
                command.Parameters.AddWithValue("@5", instructor);
                command.Parameters.AddWithValue("@6", salon);
                command.Parameters.AddWithValue("@7", letter);
                command.Parameters.AddWithValue("@8", codeQR);
                command.Parameters.AddWithValue("@9", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Boolean DeleteReservation(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "DELETE FROM Reservation WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<Reservation> ToListSalon() {
            SqlConnection connection = Connection_DB.getConnection();
            List<Reservation> reservationList = new List<Reservation>();
            try {
                connection.Open();
                String sql = "SELECT R.salon,S.number, R.id FROM Reservation As R INNER JOIN Salon As S ON R.salon = S.id";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                while (data.Read()) {
                    Reservation reservation = new Reservation();
                    reservation.id = Convert.ToInt32(data["id"]);
                    reservation.salon = Convert.ToInt32(data["salon"]);
                    reservation.letter = data["number"].ToString();
                    reservationList.Add(reservation);
                }
                connection.Close();
                return reservationList;
            }
            catch (Exception e) {
                return null;
            }
        }

        public Boolean EditReservationIncident(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE Reservation SET incident = @1 WHERE id = @2";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                command.Parameters.AddWithValue("@2", this.id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<Reservation> ListReservationIncident()
        {
            SqlConnection connection = Connection_DB.getConnection();
            List<Reservation> reservationList = new List<Reservation>();
            try
            {
                connection.Open();
                String sql = "SELECT R.id, I.status, I.type, R.incident FROM Reservation As R INNER JOIN Incident As I ON R.incident = I.id";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                while (data.Read()) {
                    Reservation reservation = new Reservation();
                    reservation.id = Convert.ToInt32(data["id"]);
                    reservation.status = Convert.ToInt32(data["status"]);
                    reservation.salon = Convert.ToInt32(data["type"]);
                    reservation.incident = Convert.ToInt32(data["incident"]);
                    reservationList.Add(reservation);
                }
                connection.Close();
                return reservationList;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Reservation> ListReservationCompleted(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            List<Reservation> reservationList = new List<Reservation>();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Reservation WHERE status = 0 AND instructor = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                SqlDataReader data = command.ExecuteReader();
                while (data.Read())
                {
                    Reservation reservation = new Reservation();
                    reservation.id = Convert.ToInt32(data["id"]);
                    reservation.hour = data["hora"].ToString();
                    reservation.date = data["fecha"].ToString();
                    reservation.status = Convert.ToInt32(data["status"]);
                    reservation.activity = data["activity"].ToString();
                    reservation.salon = Convert.ToInt32(data["salon"]);
                    reservationList.Add(reservation);
                }
                connection.Close();
                return reservationList;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public String getQR(int id) {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT qr FROM Reservation WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                String qr = command.ExecuteScalar().ToString();
                connection.Close();
                return qr;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean EditReservationUploadPresentation()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE Reservation SET presentation = @1 WHERE id = @2";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", presentation);
                command.Parameters.AddWithValue("@2", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Boolean EditReservationQuestionnaire()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE Reservation SET questionnaire = @1 WHERE id = @2";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", questionnaire);
                command.Parameters.AddWithValue("@2", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<Reservation> ActivitiesList()
        {
            SqlConnection connection = Connection_DB.getConnection();
            List<Reservation> reservationList = new List<Reservation>();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Reservation WHERE status = 0";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                while (data.Read())
                {
                    Reservation reservation = new Reservation();
                    reservation.id = Convert.ToInt32(data["id"]);
                    reservation.instructor = Convert.ToInt32(data["instructor"]);
                    reservation.activity = data["activity"].ToString();
                    reservation.salon = Convert.ToInt32(data["salon"]);
                    reservationList.Add(reservation);
                }
                connection.Close();
                return reservationList;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean InsertEnroll(int idUser)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "INSERT INTO Reservation_User(reservation, student, asistencia)"
                                + "VALUES (@1,@2,@3)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                command.Parameters.AddWithValue("@2", idUser);
                command.Parameters.AddWithValue("@3", 0);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}