﻿using Administrative_Management.DB;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Administrative_Management.Models
{
    public class LendInput
    {

        public int id { get; set; }
        public int status { get; set; }
        public int usuario { get; set; }
        public int input { get; set; }
        public int incident { get; set; }

        public Boolean InsertLendInput()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "INSERT INTO LendInput(status, usuario, input)"
                                + "VALUES (@1,@2,@3)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", status);
                command.Parameters.AddWithValue("@2", usuario);
                command.Parameters.AddWithValue("@3", input);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Boolean ChangeStatusReturn(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT id FROM LendInput WHERE input = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                int idLend = Convert.ToInt32(command.ExecuteScalar()); 
                String sql2 = "UPDATE LendInput SET status = 0 WHERE id = @2";
                SqlCommand command2 = new SqlCommand(sql2, connection);
                command2.Parameters.AddWithValue("@2", idLend);
                command2.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Boolean ChangeStatusIncident(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT id FROM LendInput WHERE input = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                int idLend = Convert.ToInt32(command.ExecuteScalar());
                String sql2 = "UPDATE LendInput SET status = 2 WHERE id = @2";
                SqlCommand command2 = new SqlCommand(sql2, connection);
                command2.Parameters.AddWithValue("@2", idLend);
                command2.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}