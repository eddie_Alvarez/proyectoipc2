﻿using Administrative_Management.DB;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Administrative_Management.Models
{
    public class User
    {

        public int license { get; set; }
        public String name { get; set; }
        public String lastName { get; set; }
        public String birthdate { get; set; }
        public String email { get; set; }
        public int phone { get; set; }
        public String username { get; set; }
        public String passsword { get; set; }
        public String keyword { get; set; }
        public int role { get; set; }


        public Boolean InsertUser()
        {

            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "INSERT INTO Usuario(license, name, last_name, birthdate, email, phone, username, contrasena, keyword, idRole)"
                                + "VALUES (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.CommandType = System.Data.CommandType.Text;
                command.Parameters.AddWithValue("@1", license);
                command.Parameters.AddWithValue("@2", name);
                command.Parameters.AddWithValue("@3", lastName);
                command.Parameters.AddWithValue("@4", birthdate);
                command.Parameters.AddWithValue("@5", email);
                command.Parameters.AddWithValue("@6", phone);
                command.Parameters.AddWithValue("@7", username);
                command.Parameters.AddWithValue("@8", passsword);
                command.Parameters.AddWithValue("@9", keyword);
                command.Parameters.AddWithValue("@10", role);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public String LoginUser()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT contrasena, idRole, license FROM Usuario" +
                    " WHERE username = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", username);
                SqlDataReader data = command.ExecuteReader();
                String getPassword = "";
                while (data.Read())
                {
                    getPassword = (data["contrasena"]).ToString();
                    role = Convert.ToInt32(data["idRole"]);
                    license = Convert.ToInt32(data["license"]);
                }
                connection.Close();
                return getPassword;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<User> ListUser()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Usuario";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                List<User> userList = new List<User>();
                while (data.Read())
                {
                    User user = new User();
                    user.license = Convert.ToInt32(data["license"]);
                    user.name = data["name"].ToString();
                    user.lastName = data["last_name"].ToString();
                    user.birthdate = data["birthdate"].ToString();
                    user.email = data["email"].ToString();
                    user.phone = Convert.ToInt32(data["phone"]);
                    user.username = data["username"].ToString();
                    user.role = Convert.ToInt32(data["idRole"]);
                    userList.Add(user);
                }
                connection.Close();
                return userList;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public User SelectUser(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Usuario WHERE license = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                SqlDataReader data = command.ExecuteReader();
                User user = new User();
                while (data.Read())
                {
                    user.license = Convert.ToInt32(data["license"]);
                    user.name = data["name"].ToString();
                    user.lastName = data["last_name"].ToString();
                    user.birthdate = data["birthdate"].ToString();
                    user.email = data["email"].ToString();
                    user.phone = Convert.ToInt32(data["phone"]);
                    user.username = data["username"].ToString();
                    user.role = Convert.ToInt32(data["idRole"]);
                }
                connection.Close();
                return user;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean EditUser()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE Usuario SET name = @1, last_name = @2, birthdate = @3, email = @4, phone = @5, username = @6 WHERE license = @7";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", name);
                command.Parameters.AddWithValue("@2", lastName);
                command.Parameters.AddWithValue("@3", birthdate);
                command.Parameters.AddWithValue("@4", email);
                command.Parameters.AddWithValue("@5", phone);
                command.Parameters.AddWithValue("@6", username);
                command.Parameters.AddWithValue("@7", license);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Boolean DeleteUser(int id) {
            SqlConnection connection = Connection_DB.getConnection();
            try {
                connection.Open();
                String sql = "DELETE FROM Usuario WHERE license = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e) {
                return false;
            }
        }

        public List<User> SearchReservation() {
            SqlConnection connection = Connection_DB.getConnection();
            List<User> userList = new List<User>();
            try {
                connection.Open();
                String sql = "SELECT license, username FROM Usuario WHERE idRole = 4";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                while (data.Read()) {
                    User user = new User();
                    user.license = Convert.ToInt32(data["license"]);
                    user.username = data["username"].ToString();
                    userList.Add(user);
                }
                connection.Close();
                return userList;
            }
            catch (Exception e) {
                return null;
            }
        }

        public User RecoverPassword() {
            SqlConnection connection = Connection_DB.getConnection();
            try {
                connection.Open();
                String sql = "SELECT contrasena, keyword, email, name FROM Usuario WHERE license = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", license);
                SqlDataReader data = command.ExecuteReader();
                User user = new User();
                while (data.Read()) {
                    user.passsword = data["contrasena"].ToString();
                    user.name = data["name"].ToString();
                    user.keyword = data["keyword"].ToString();
                    user.email = data["email"].ToString();
                }
                connection.Close();
                return user;
            }
            catch (Exception e) {
                return null;
            }
        }

        public Boolean ChangePassword() {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE Usuario SET contrasena = @1 WHERE license = @2";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", passsword);
                command.Parameters.AddWithValue("@2", license);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e) {
                return false;
            }
        }

        public List<User> ListUserForLend()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT license, username FROM Usuario WHERE idRole = 3 OR idRole = 4";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                List<User> userList = new List<User>();
                while (data.Read())
                {
                    User user = new User();
                    user.license = Convert.ToInt32(data["license"]);
                    user.username = data["username"].ToString();
                    userList.Add(user);
                }
                connection.Close();
                return userList;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}