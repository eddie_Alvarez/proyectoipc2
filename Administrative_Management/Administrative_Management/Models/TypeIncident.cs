﻿using Administrative_Management.DB;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Administrative_Management.Models
{
    public class TypeIncident
    {
        public int id { get; set; }
        public String name { get; set; }

        public List<TypeIncident> ToList() {
            SqlConnection connection = Connection_DB.getConnection();
            List<TypeIncident> typeList = new List<TypeIncident>();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM TypeIncident";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                while (data.Read())
                {
                    TypeIncident type = new TypeIncident();
                    type.id = Convert.ToInt32(data["id"]);
                    type.name = data["name"].ToString();
                    typeList.Add(type);
                }
                connection.Close();
                return typeList;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}