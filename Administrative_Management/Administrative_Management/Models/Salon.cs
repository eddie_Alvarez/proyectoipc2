﻿using Administrative_Management.DB;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Administrative_Management.Models
{
    public class Salon
    {
        public int id { get; set; }
        public String number { get; set; }
        public String location { get; set; }
        public int status { get; set; }

        public Boolean InsertSalon()
        {

            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "INSERT INTO Salon(number, location, status)"
                                + "VALUES (@1,@2,@3)";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", number);
                command.Parameters.AddWithValue("@2", location);
                command.Parameters.AddWithValue("@3", status);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public List<Salon> ListSalon()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Salon";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                List<Salon> salonList = new List<Salon>();
                while (data.Read())
                {
                    Salon salon = new Salon();
                    salon.id = Convert.ToInt32(data["id"]);
                    salon.number = data["number"].ToString();
                    salon.location = data["location"].ToString();
                    salon.status = Convert.ToInt32(data["status"]);
                    salonList.Add(salon);
                }
                connection.Close();
                return salonList;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Salon SelectSalon(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "SELECT * FROM Salon WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                SqlDataReader data = command.ExecuteReader();
                Salon salon = new Salon();
                while (data.Read())
                {
                    salon.id = Convert.ToInt32(data["id"]);
                    salon.number = data["number"].ToString();
                    salon.location = data["location"].ToString();
                    salon.status = Convert.ToInt32(data["status"]);
                }
                connection.Close();
                return salon;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean EditSalon()
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "UPDATE Salon SET number = @1, location = @2, status = @3 WHERE id = @4";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", number);
                command.Parameters.AddWithValue("@2", location);
                command.Parameters.AddWithValue("@3", status);
                command.Parameters.AddWithValue("@4", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Boolean DeleteSalon(int id)
        {
            SqlConnection connection = Connection_DB.getConnection();
            try
            {
                connection.Open();
                String sql = "DELETE FROM Salon WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<Salon> SearchReservation() {
            SqlConnection connection = Connection_DB.getConnection();
            List<Salon> salonList = new List<Salon>();
            try {
                connection.Open();
                String sql = "SELECT id, number FROM Salon";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader data = command.ExecuteReader();
                while (data.Read()) {
                    Salon salon = new Salon();
                    salon.id = Convert.ToInt32(data["id"]);
                    salon.number = data["number"].ToString();
                    salonList.Add(salon);
                }
                connection.Close();
                return salonList;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean ChangeStatusReservation(int id) {
            SqlConnection connection = Connection_DB.getConnection();
            try {
                connection.Open();
                String sql = "UPDATE Salon SET status = 2 WHERE id = @1";
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@1", id);
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception e) {
                return false;
            }
        }
    }
}