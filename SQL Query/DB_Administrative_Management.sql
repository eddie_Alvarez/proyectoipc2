CREATE DATABASE DB_Administrative_Management;
GO
USE DB_Administrative_Management;
GO
CREATE TABLE Role(
	id int PRIMARY KEY IDENTITY(1,1) NOT NULL,
	name varchar(50) NOT NULL,
	)
GO
CREATE TABLE Usuario(
	license int PRIMARY KEY,
	name varchar(100) NOT NULL,
	last_name varchar(100) NOT NULL,
	birthdate varchar(150) NOT NULL,
	email varchar(300) NOT NULL,
	phone int,
	username varchar(500) NOT NULL,
	contrasena varchar(1000) NOT NULL,
	keyword varchar(30) NOT NULL,
	idRole int FOREIGN KEY REFERENCES Role(id)
	)
GO
CREATE TABLE Salon(
	id int PRIMARY KEY IDENTITY(1,1),
	number VARCHAR(300) NOT NULL,
	location VARCHAR(60) NOT NULL,
	status int NOT NULL
)
GO
CREATE TABLE TypeIncident(
	id int PRIMARY KEY IDENTITY(1,1),
	name VARCHAR(80) NOT NULL
)
GO
CREATE TABLE Incident(
	id int PRIMARY KEY IDENTITY(1,1),
	description VARCHAR(500) NOT NULL,
	status int NOT NULL,
	type int FOREIGN KEY REFERENCES TypeIncident(id),
)
GO
CREATE TABLE Reservation(
	id INT PRIMARY KEY IDENTITY(1,1),
	hora VARCHAR(50) NOT NULL,
	fecha VARCHAR(50) NOT NULL,
	qr VARCHAR(MAX),
	status int NOT NULL,
	letter VARCHAR(200),
	activity VARCHAR(100) NOT NULL,
	questionnaire VARCHAR(1000),
	presentation VARCHAR(1000),
	operator int FOREIGN KEY REFERENCES Usuario(license),
	instructor int FOREIGN KEY REFERENCES Usuario(license),
	salon int FOREIGN KEY REFERENCES Salon(id),
	incident int FOREIGN KEY REFERENCES Incident(id)
	)
GO

CREATE TABLE TypeInput(
	id INT PRIMARY KEY IDENTITY(1,1),
	name VARCHAR(100) NOT NULL
)
GO

CREATE TABLE Input(
	id INT PRIMARY KEY IDENTITY(1,1),
	name VARCHAR(100) NOT NULL,
	brand VARCHAR(200) NOT NULL,
	model VARCHAR(200) NOT NULL,
	status int,
	type int FOREIGN KEY REFERENCES TypeInput(id)
)
GO

CREATE TABLE LendInput(
	id INT PRIMARY KEY IDENTITY(1,1),
	status int NOT NULL,
	usuario int FOREIGN KEY REFERENCES Usuario(license),
	input int FOREIGN KEY REFERENCES Input(id),
	incident INT FOREIGN KEY REFERENCES Incident(id)
)
GO

CREATE TABLE Reservation_User(
	reservation INT FOREIGN KEY REFERENCES Reservation(id),
	student INT FOREIGN KEY REFERENCES Usuario(license),
	asistencia INT,
)

INSERT INTO Role(name)
VALUES('administrador')
INSERT INTO Role(name)
VALUES('operador')
INSERT INTO Role(name)
VALUES('estudiante')
INSERT INTO Role(name)
VALUES('instructor')
GO
INSERT INTO TypeIncident(name)
VALUES('Escritorios da�ados')
INSERT INTO TypeIncident(name)
VALUES('Pizarra da�ada')
INSERT INTO TypeIncident(name)
VALUES('Paredes da�adas')
INSERT INTO TypeIncident(name)
VALUES('Piso da�ado')
GO
INSERT INTO Usuario(license, name, last_name, birthdate, email, phone, username, contrasena, keyword, idRole)
VALUES(0,'admin', 'admin', '0', 'admin@admin.com', 0,'admin', 'admin', 'admin', 1)
GO
INSERT INTO TypeInput(name)
VALUES('Computadoras')
INSERT INTO TypeInput(name)
VALUES('Cables')
INSERT INTO TypeInput(name)
VALUES('Ca�oneras')
INSERT INTO TypeInput(name)
VALUES('Otros')